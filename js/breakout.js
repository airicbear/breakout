let keysDown = {};

addEventListener("keydown", function (e) {
  keysDown[e.keyCode] = true;
}, false);

addEventListener("keyup", function (e) {
  delete keysDown[e.keyCode];
}, false);

/**
 * Return -1 or 1 based on user input
 * @param {string} axis 
 */
function GetAxis(axis) {
  switch (axis) {
    case "Horizontal":
      if (37 in keysDown || 65 in keysDown) {
        return -1;
      } else if (39 in keysDown || 68 in keysDown) {
        return 1;
      }
    case "Vertical":
      if (38 in keysDown || 87 in keysDown) {
        return -1;
      } else if (40 in keysDown || 83 in keysDown) {
        return 1;
      }
    default:
      return 0;
  }
}

/**
 * Basic 2-dimensional vector.
 * @param {number} x 
 * @param {number} y 
 */
function Vector2(x, y) {
  this.x = x;
  this.y = y;
  this.add = (other) => new Vector2(this.x + other.x, this.y + other.y);
  this.multiply = (n) => new Vector2(this.x * n, this.y * n);
  this.equals = (other) => this.x === other.x && this.y === other.y;
  this.toString = () => "<" + this.x + ", " + this.y + ">";
}

/**
 * Create a HTML5 canvas.
 * @param {number} width 
 * @param {number} height 
 * @param {string} backgroundColor 
 */
function createCanvas(width = 500, height = 500, backgroundColor = 'black') {
  let canvas = document.createElement("canvas");
  canvas.width = width;
  canvas.height = height;
  canvas.ctx = canvas.getContext("2d");
  canvas.drawBackground = (color = backgroundColor) => {
    canvas.ctx.fillStyle = color;
    canvas.ctx.fillRect(0, 0, canvas.width, canvas.height);
  };
  canvas.drawBackground("black");
  return canvas;
}

/**
 * Basic GameObject.
 * @param {Vector2} position 
 * @param {Vector2} scale 
 * @param {Vector2} velocity 
 * @param {string} color 
 */
function GameObject(position = new Vector2(0, 0), scale = new Vector2(0, 0), velocity = new Vector2(0, 0), speed = 5, color = 'white') {
  this.position = position;
  this.scale = scale;
  this.velocity = velocity;
  this.speed = speed;
  this.color = color;
  this.move = () => {
    this.position = this.position.add(this.velocity.multiply(this.speed));
  };
  this.draw = (ctx) => {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.position.x, this.position.y, this.scale.x, this.scale.y);
  };
  this.isOutOfBoundsX = (canvas) => this.position.x < 0 || this.position.x + this.scale.x > canvas.width;
  this.isOutOfBoundsY = (canvas) => this.position.y < 0 || this.position.y + this.scale.y > canvas.height;
  this.isOutOfBounds = (canvas) => this.isOutOfBoundsX(canvas) || this.isOutOfBoundsY(canvas);
  this.stayInBounds = (canvas) => {
    if (this.isOutOfBounds(canvas)) {
      this.position = this.position.add(this.velocity.multiply(-this.speed));
    }
  };
  this.collidingX = (other) => this.position.x < other.position.x + other.scale.x && this.position.x + this.scale.x > other.position.x;
  this.collidingY = (other) => this.position.y < other.position.y + other.scale.y && this.position.y + this.scale.y > other.position.y;
  this.colliding = (other) => this.collidingX(other) && this.collidingY(other);
  this.bounceX = () => {
    this.velocity = new Vector2(-this.velocity.x, this.velocity.y);
  };
  this.bounceY = () => {
    this.velocity = new Vector2(this.velocity.x, -this.velocity.y);
  };
  this.bounce = (other) => {
    if (this.collidingY(other)) {
      this.bounceY();
    } else if (this.collidingX(other)) {
      this.bounceX();
    }
  }
  this.update = () => 0;
}

const Breakout = {
  "main": function () {
    this.start();
    this.update();
  },
  "start": function () {
    this.canvas = createCanvas();
    document.body.appendChild(this.canvas);

    let paddle, ball, bricks;

    // Paddle properties
    const paddleWidth = 60;
    const paddleHeight = 5;
    let initPosY = this.canvas.height * 2 - 10;

    // Paddle
    let initPosX1 = this.canvas.width / 2 - paddleWidth / 2;
    paddle = new GameObject(new Vector2(initPosX1, initPosY), new Vector2(paddleWidth, paddleHeight));
    paddle.lives = 3;
    paddle.speed *= 1.5;
    paddle.update = () => {
      paddle.scale.x = this.canvas.width / 20;

      // Keep paddle in screen
      paddle.position.y = this.canvas.height - paddleHeight - 10;

      paddle.velocity = new Vector2(GetAxis("Horizontal"), 0);;
    };

    // Bricks
    let createBricks = () => {
      let bricks = [];
      let numBrickRow = 10;
      let numBrickCol = 15;
      let brickPadding = 25;
      let brickSpacing = 1;
      for (let r = 0; r < numBrickRow; r++) {
        for (let c = 0; c < numBrickCol; c++) {        
          let brick = new GameObject();
          brick.destroy = () => brick.destroyed = true;
          brick.update = () => {
            if (!brick.destroyed) {
              brick.scale = new Vector2(this.canvas.width / numBrickCol - brickPadding / 6, numBrickCol);
              let brickPosition = new Vector2(c * brick.scale.x + c * brickSpacing + brickPadding, r * brick.scale.y + r * brickSpacing + brickPadding);
              brick.position = brickPosition;
            } else {
              brick.scale = new Vector2(0, 0);
              brick.update = () => 0;
            }
            if (ball.colliding(brick)) {
              ball.bounce(brick);
              brick.destroyed = true;
            }
          }
          bricks.push(brick);
        }
      }
      return bricks;  
    }
    bricks = createBricks();
    
    // Ball
    const ballScale = new Vector2(10, 10);
    let ballPosition = new Vector2(this.canvas.width / 2 - ballScale.x / 2, this.canvas.height);
    ball = new GameObject(ballPosition, ballScale);
    ball.reset = () => {
      ball.speed = 5;

      // Direction of the ball
      const dirX = [-1, 1];

      // Reset ball position
      ball.position =  new Vector2(Math.random() * (this.canvas.width / 2 - ballScale.x / 2), (this.canvas.height * 2- ball.scale.y));

      // Randomize ball velocity
      ball.velocity = new Vector2(dirX[Math.round(Math.random() * (dirX.length - 1))], -1);
    }
    ball.reset();
    ball.stayInBounds = (canvas) => {
      if (ball.isOutOfBoundsX(canvas)) {
        ball.bounceX();
      } else if (ball.isOutOfBoundsY(canvas)) {
        if (ball.position.y > canvas.height) {
          if (paddle.lives > 0) {
            paddle.lives--;
          } else {
            if (confirm("Game Over. Retry?")) {
              window.location.reload(false);
            } else {
              this.gameObjects.pop(ball);
            }
          }
          ball.reset();
        } else if (ball.position.y + ball.scale.y < canvas.height) {
          ball.bounceY();
        }
      }
    };
    ball.update = () => {
      if (ball.colliding(paddle)) {
        ball.bounceY();
      }
    };

    this.gameObjects.push(paddle);
    for (let i = 0; i < bricks.length; i++) {
      this.gameObjects.push(bricks[i]);
    }
    this.gameObjects.push(ball);
  },
  "update": function () {
    window.requestAnimationFrame(() => this.update());
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.canvas.drawBackground();
    for (let i = 0; i < this.gameObjects.length; i++) {
      const gameObj = this.gameObjects[i];
      gameObj.draw(this.canvas.ctx);
      gameObj.update();
      gameObj.move();
      gameObj.stayInBounds(this.canvas);

      // Keep game objects in screen
      if (gameObj.position.y + gameObj.scale.y > this.canvas.height + 10) {
        gameObj.position.y = this.canvas.height - 20 - gameObj.scale.y;
      }
      if (gameObj.position.y < -10) {
        gameObj.position.y = 50;
      }
      if (gameObj.position.x + gameObj.scale.x > this.canvas.width) {
        gameObj.position.x = this.canvas.width - gameObj.scale.x - 10;
      }
    }
  },
  "gameObjects": [],
}